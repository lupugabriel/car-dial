# CarDial v0.0.1 #

### Instalarea aplicatiei ###

**Cerinte**

* node.js ( v0.10.x )
* npm ( v1.3.x )
* bower ( v1.3.x )

*Este posibil sa functioneze si alte versiuni, versiunile mai sus mentionate au fost folosite in procesul de dev*

https://bitbucket.org/lupugabriel/car-dial/src

Instalarea dependintelor necesare se face cu


```
#!javascript

> npm install && bower install
```


In root-ul aplicatiei

### Rularea serverului ###


```
#!javascript

> grunt 
```

Task-ul default care creeaza resursele necesare pentru rularea aplicatiei in mediul de development

```
#!javascript

> grunt deploy
```

Task-ul care creeaza resursele necesare pentru mediul de productie ( ruleaza si grunt implicit )


```
#!javascript

> node server.js
```
Rularea serverului in mod implicit preia resursele din ./deploy si se face pe portul 81.
Se pot aplica parametrii --dev --port=8080 spre exemplu pentru a rula versiunea de development pe portul 8080. node server.js --help pentru lista completa
ex : 
```
#!javascript

> node server.js --dev --port=8080
```

## Extra-functionalitati ##

* Integrare cu Google maps ( prima rulare solicita permisiuni de preluare a pozitiei utilizatorului cu HTML5 geolocation pentru centrarea hartii )
* Pictograme pentru toate contactele ( contacts_list.png ) - /#contacts-list/1
* Afisarea locatiei utilizatorului ( nume ) folosind servicii Google ( contact_view_features.png ) #contacts/9
* Calcularea rutei catre un contact folosing Google maps ( contact_view_features.png, contact_route.png ) - Functioneaza pentru "Elton Montgomery", majoritatea coordonatelor pentru contacte fiind in oceane sau in afara hartii.
* Afisarea contactelor pe harta "Contacts Map" - Click pe contact deschide contact view modal ( contacts_map.png ) - /#map/contacts

---

# Cerinta initiala: #

# Application requirements

>Create an in-car application that “calls” someone from your list of contacts.
The list of contacts is provided as contacts.json.
 
##Here’s how the application would work:

1. Driver sees screen with a “Place call” button.
2. Clicking that button will show an overlay with a list of contacts (from contacts.json)
3. The list of contacts is sortable, searchable and paginated (10 items per page max).
4. User scrolls, searches etc and finally selects an entry.
5. Modal closes and the Call Screen is shown – think Android or iPhone etc call screen but with some extra things.
6. Call Screen displays information about the selected user:

 * name
 * country and city
 * telephone number
 * days until next birthday or “Don’t forget to wish you friend a happy birthday today.” The birthdate is a UNIX timestamp (seconds).
 * small map of the last recorded position. This is available as `currentCoordinates` in the contacts.json file and represents a pair of (latitude, longitude) coordinates. Coordinates will be all over the place, but you tend to have many friends when you have a cool car.
 
 
*Optional: Use a Front-End build system to build make your app ready for production.*