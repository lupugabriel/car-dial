define(function(require, exports, module) {
    var app = require('app');
    var MapController = require('modules/map/controllers/map');

    var GoogleMapInitializer = Marionette.Controller.extend({
        initialize: function() {
            app.on('initialize:after', function(opions){
                this.map = new MapController();
            });
        }
    });

    module.exports = new GoogleMapInitializer();
});
