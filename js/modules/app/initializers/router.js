define(function(require, exports, module) {
    var app = require('app');
    var MapRouter = require('modules/map/router');
    var CallRouter = require('modules/call/router');
    var ContactsRouter = require('modules/contacts/router');

    var RouterInitializer = Marionette.Controller.extend({
        initialize: function() {
            app.on('initialize:after', function(opions){
                new MapRouter();
                new CallRouter();
                new ContactsRouter();
            });
        }
    });

    module.exports = new RouterInitializer();
});
