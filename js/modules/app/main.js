define(function(require, exports, module) {
    require('./initializers/show-layout');
    require('./initializers/google-map');
    require('./initializers/router');
});