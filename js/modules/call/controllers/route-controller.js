define(function(require, exports, module) {
    var Marionette = require('marionette');
    var app = require('app');

    var ContactCollection = require('modules/contacts/collections/contacts');
    var CallLayout = require('../views/layout');

    var CallController = Marionette.Controller.extend({
        call: function ( contactId ) {
            app.map.hideMap();
            app.navbar.setActiveMenuItem('call');
            // The contact model should have been fetched
            // from a /api/contacts/<id>, not filtering
            // the entire collection
            var contacts = new ContactCollection();
            contacts.filterContactId = contactId;

            contacts.fetch({
                success: function( collection, response ) {
                    app.showLayout( new CallLayout({
                        model: collection.fullCollection.where({
                                id: parseInt( collection.filterContactId )
                            })[0]
                    }));
                }
            })
        }
    });

    module.exports = new CallController();
});