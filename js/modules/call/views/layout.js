define(function(require, exports, module) {
    var Backbone = require('backbone');
    var Marionette = require('marionette');

    var DateController = require('modules/dates/controllers/date');

    module.exports = Marionette.ItemView.extend({
        id: 'callContact',
        template: '#call-layout',
        ui: {
            $dots: '.dots',
            $calling: '.calling',
            $endCall: '#endCall'
        },
        events: {
            'click @ui.$endCall': 'endCall'
        },
        endCall: function() {
            var self = this;
            self.ui.$calling.html('Call ended');

            setTimeout(function(){
                Backbone.history.history.back();
                self.close();
            }, 1000);
        },
        onRender: function() {
            var self = this;
            var i = 0;
            var dotsInterval = setInterval(function(){
                // str.repeat is proposal for ECMAScript 6
                self.ui.$dots.html( Array( ( i > 4 ) ? ( i = 0 ) : ( i++ ) ).join( "." ) );
            }, 300);

            this.on('close', function() {
                clearInterval( dotsInterval );
            })
        },
        templateHelpers: function() {
            return {
                getBirthday: function() {
                    var dateController = new DateController( this.model.birthDate );
                    return dateController.getBirthday();
                }
            }
        }
    });
});