define(function(require, exports, module) {
    var Backbone = require('backbone');
    var ContactModel = require('../models/contact');

    module.exports = Backbone.PageableCollection.extend({
        url: '/api/contacts.json',
        model: ContactModel,
        state: {
            pageSize: 10
        },
        mode: 'client'
    });
});