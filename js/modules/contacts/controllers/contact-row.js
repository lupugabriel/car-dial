define(function(require, exports, module) {
    var Backbone = require('backbone');
    var Backgrid = require('backgrid');
    var app = require('app');

    var ContactView = require('../views/contact');

    module.exports = Backgrid.Row.extend({
        events: {
            click: 'contactClicked'
        },
        contactClicked: function() {
            Backbone.history.navigate( 'contacts/' + this.model.id, true );
        }
    });
});