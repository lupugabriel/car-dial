define(function(require, exports, module) {
    var Backgrid = require('backgrid');

    module.exports = Backgrid.Cell.extend({
        className: 'image-cell',
        render: function () {
            this.$el.empty();
            this.$el.html( this.renderImage( this.model ) );
            this.delegateEvents();
            return this;
        },
        renderImage: function( model ) {
            return '<img src="api/static/avatar/' + this.model.id + '.jpg" class="img-circle" />';
        }
    });
});