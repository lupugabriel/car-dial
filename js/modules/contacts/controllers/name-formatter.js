define(function(require, exports, module) {
    var Backgrid = require('backgrid');

    module.exports = _.extend(Backgrid.CellFormatter, {
        fromRaw: function (rawValue, model) {
            return model.get('firstName') + ' ' + model.get('lastName')
        }
    });
});