define(function(require, exports, module) {
    var Marionette = require('marionette');
    var app = require('app');

    var ListView = require('../views/list');
    var ContactView = require('../views/contact');
    var ContactCollection = require('../collections/contacts');

    var CallController = Marionette.Controller.extend({
        contactsList: function( page ) {
            app.navbar.setActiveMenuItem('call');
            app.map.hideMap();
            app.showLayout( new ListView({
                pageNumber: page
            }) );
        },
        viewContact: function( contactId ) {
            var self = this;
            // The contact model should have been fetched
            // from a /api/contacts/<id>, not filtering
            // the entire collection
            var contacts = new ContactCollection();
            contacts.filterContactId = contactId;

            contacts.fetch({
                success: function( collection, response ) {
                    var contactModel = collection.fullCollection.where({
                        id: parseInt( collection.filterContactId )
                    })[0];

                    self.showContactModal( contactModel )
                }
            });
        },
        showContactModal: function( model ) {
            var contactView = new ContactView({
                model: model
            });

            app.modal.showModal({
                title: {
                    icon: 'user',
                    label: model.get('firstName') + ' ' + model.get('lastName')
                },
                body: contactView,
                buttons: new Backbone.Collection([
                {
                    type: 'success',
                    label: 'Call ' + model.get('firstName'),
                    icon: 'earphone',
                    click: _.bind(function() {
                        Backbone.history.navigate('call/' + model.id, true);
                        app.modal.hideModal();
                    }, this)
                },
                {
                    type: 'primary',
                    label: 'Drive to ' + model.get('firstName'),
                    icon: 'road',
                    click: _.bind(function() {
                        var currentPostion = model.getPosition();
                        app.map.driveTo( currentPostion.lat, currentPostion.lng );
                    }, this)
                }
                ])
            });
        }
    });

    module.exports = new CallController();
});