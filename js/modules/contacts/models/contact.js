define(function(require, exports, module) {
    var Backbone = require('backbone');
    var app = require('app');

    module.exports = Backbone.Model.extend({
        getPosition: function() {
            var position = this.get('currentCoordinates').split(",");
            return {
                lat: position[0],
                lng: position[1]
            }
        },
        addToMap: function() {
            var position = this.getPosition();

            var marker = new google.maps.Marker({
                 position: new google.maps.LatLng(position.lat, position.lng),
                 icon: 'api/static/avatar/' + this.id + '.jpg'
            });

            marker.contactId = this.id;

            marker.setMap( app.map.map )

            google.maps.event.addListener(marker, 'click', function() {
                Backbone.history.navigate('contacts/' + this.contactId, true);
            });
        }
    });
});