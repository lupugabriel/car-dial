define(function(require, exports, module) {
    var Marionette = require('marionette');
    var app = require('app');

    var DateController = require('modules/dates/controllers/date');

    module.exports = Marionette.ItemView.extend({
        id: 'viewContact',
        template: '#contacts-contact',
        ui: {
            $directionsError: '#directionsError',
            $lastSeenLocationName: '#lastSeenLocationName'
        },
        onRender: function() {
            this.listenTo( app.map, 'direction:invalid', function(){
                this.ui.$directionsError.html('Cannot calculate route to ' + this.model.get('firstName') + '\'s current position');
            });

            var contactLastSeen = this.model.getPosition();

            app.map.getLocationName( contactLastSeen.lat, contactLastSeen.lng, _.bind( function( name ){ 
                this.ui.$lastSeenLocationName.html( name );
            }, this ) );
        },
        templateHelpers: function() {
            return {
                getUserLastSeen: function() {
                    return this.model.currentCoordinates.replace(' ', '');
                },
                getBirthday: function() {
                    var dateController = new DateController( this.model.birthDate );
                    return dateController.getBirthday();
                }
            }
        }
    });
});