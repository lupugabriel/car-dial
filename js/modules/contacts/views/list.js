define(function(require, exports, module) {
    var Marionette = require('marionette');
    var Backgrid = require('backgrid');
    var BackgridFilter = require('backgrid-filter');
    var BackgridPaginator = require('backgrid-paginator');

    var ContactsCollection = require('../collections/contacts');
    var NameFormatter = require('../controllers/name-formatter');
    var ImageCell = require('../controllers/image-cell');
    var ContactRow = require('../controllers/contact-row');

    module.exports = Marionette.Layout.extend({
        template: '#contacts-list',
        regions: {
            filter: '#contactsFilter',
            table: '#contactsTable',
            pagination: '#contactsPagination'
        },
        onRender: function() {
            var contacts = new ContactsCollection();
            var pageableGrid = new Backgrid.Grid({
                row: ContactRow,
                columns: [
                {
                    name: 'id',
                    editable: false,
                    label: '',
                    cell: ImageCell
                }
                ,{
                    name: 'firstName',
                    editable: false,
                    label: 'Name',
                    cell: 'string',
                    formatter: NameFormatter
                },
                {
                    name: 'lastName',
                    editable: false,
                    renderable: false,
                    label: 'Last Name',
                    cell: 'string'
                },
                {
                    name: 'country',
                    editable: false,
                    label: 'Country',
                    cell: 'string'
                },
                {
                    name: 'city',
                    editable: false,
                    label: 'City',
                    cell: 'string'
                }],
                collection: contacts,
                emptyText: "No Contacts Found"
            });

            var paginator = new Backgrid.Extension.Paginator({
              collection: contacts
            });

            var filter = new Backgrid.Extension.ClientSideFilter({
                collection: contacts,
                fields: ['firstName', 'lastName', 'country', 'city']
            });

            this.filter.show( filter );
            this.table.show( pageableGrid );
            this.pagination.show( paginator );

            contacts.fetch({reset: true});

            if ( this.options.pageNumber ) {
                this.listenTo( contacts, 'sync', function(){
                    contacts.getPage( parseInt( this.options.pageNumber ) );
                })
            }

            this.listenTo( contacts, 'reset', function(){
                Backbone.history.navigate('contacts-list/' + contacts.state.currentPage);
            });
        }
    });
});