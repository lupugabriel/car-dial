define(function(require, exports, module) {
    var Marionette = require('marionette');

     module.exports = Marionette.Controller.extend({
        initialize: function( birthDateTimestamp ) {
            var birthDate = this.birthDate = new Date( birthDateTimestamp * 1000 );
            birthDate.setHours(0, 0, 0, 0);
            this.birthDateDay = birthDate.getDate();
            this.birthDateMonth = birthDate.getMonth();
        },
        getBirthday: function () {
            var now = new Date();
            now.setHours(0, 0, 0, 0);
            var birthDay = new Date( this.birthDate );
            birthDay.setFullYear( now.getFullYear() );

            if ( now > birthDay ) {
                birthDay.setFullYear( now.getFullYear() + 1 );
            }

            var daysDiff = Math.floor(( birthDay - now ) / ( 1000 * 3600 * 24 ));

            if ( !daysDiff ) {
                return '<strong>Don’t forget to wish your friend a happy birthday today.</strong>';
            } else {
                return daysDiff + ' days until birthday!';
            }
        },
        getMonthName: function( month ) {
            var months = ["January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ];

            return months[ this.month ];
        }
    });
});