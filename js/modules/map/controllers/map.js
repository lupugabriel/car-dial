define(function(require, exports, module) {
    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    var Marionette = require('marionette');
    var app = require('app');

    module.exports = Marionette.Controller.extend({
        carLocation: {
            lat: 44.429584,
            lng: 26.035731
        },
        initialize: function () {
            var self = this;

            var map = this.map = this.getGoogleMap();

            if ( navigator.geolocation ) {
                navigator.geolocation.getCurrentPosition(function( position ) {
                    var clientLocation = new google.maps.LatLng( position.coords.latitude, position.coords.longitude );
                    map.setCenter( clientLocation );
                    self.carLocation.lat = position.coords.latitude;
                    self.carLocation.lng = position.coords.longitude;
                });
            }

            google.maps.event.addListenerOnce( map, 'idle', function(){
                self.mapLoaded();
            });

            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer();
            this.geoCoder = new google.maps.Geocoder();

            this.directionsDisplay.setMap( map );
        },
        getGoogleMap: function() {
            return new google.maps.Map($('#map_canvas')[0], {
                center: new google.maps.LatLng(this.carLocation.lat, this.carLocation.lng),
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
            });
        },
        mapLoaded: function() {
            var currentPath = Backbone.history.getHash();
            if ( currentPath !== 'map' && currentPath !== 'map/contacts' && currentPath !== '' ) {
                this.hideMap();
            }
        },
        showMap: function() {
            $('#map-container .gm-style div:first').removeClass('blur-item');
        },
        hideMap: function() {
            $('#map-container .gm-style div:first').addClass('blur-item');
        },
        setZoom: function( zoom ) {
            this.map.setZoom( zoom );
        },
        driveTo: function(latitude, longitude) {
            var self = this;

            var request = {
                origin: new google.maps.LatLng( latitude, longitude ),
                destination: new google.maps.LatLng( this.carLocation.lat, this.carLocation.lng ),
                travelMode: google.maps.TravelMode['DRIVING']
            }

            this.directionsService.route( request, function( response, status ) {
                if ( status === google.maps.DirectionsStatus.OK ) {
                    self.directionsDisplay.setDirections( response );
                    Backbone.history.navigate('/', true);
                    app.modal.hideModal();
                } else {
                    self.trigger('direction:invalid');
                }
            });
        },
        getLocationName: function(latitude, longitude, callback) {
            var locationPosition = new google.maps.LatLng( latitude, longitude );
            this.geoCoder.geocode({
                'latLng': locationPosition
            }, function( results, status ) {
                if ( status === google.maps.GeocoderStatus.OK ) {
                    callback( results[0].formatted_address );
                } else {
                    callback( 'Unknown Location' )
                }
            });
        },
        centerOnClient: function() {
            var clientLocation = new google.maps.LatLng( this.carLocation.lat, this.carLocation.lng );
            this.map.setCenter( clientLocation );
            this.setZoom( 17 );
        }
    });
});