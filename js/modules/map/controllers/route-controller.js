define(function(require, exports, module) {
    var Marionette = require('marionette');
    var app = require('app');

    var ContactCollection = require('modules/contacts/collections/contacts');

    var MapRouteController = Marionette.Controller.extend({
        showMap: function () {
            app.navbar.setActiveMenuItem('showmap');
            app.closeLayout();
            app.map.showMap();
            app.map.centerOnClient();
        },
        showContactsMap: function() {
            app.map.showMap();
            app.closeLayout();
            app.navbar.setActiveMenuItem('contactsmap');

            var contacts = new ContactCollection();
            contacts.fetch({
                success: function(collection) {
                    collection.each(function(model){
                        model.addToMap();
                    })
                }
            });

            app.map.setZoom(3);
        }
    });

    module.exports = new MapRouteController();
});