define(function(require, exports, module) {
    var Marionette = require('marionette');
    var RouteController = require('modules/map/controllers/route-controller');

    module.exports = Marionette.AppRouter.extend({
        controller: RouteController,
        appRoutes: {
            // our default route
            'map': 'showMap',
            'map/contacts': 'showContactsMap',
            '*path': 'showMap'
        }
    });
});