define(function(require, exports, module) {
    var $ = require('jquery');
    var Marionette = require('marionette');

    module.exports = Marionette.ItemView.extend({
        template: '#modal-view-buttons-item',
        tagName: 'button',
        events: {
            'click': 'buttonClick'
        },
        className: function() {
            return 'btn btn-' + this.model.get('type');
        },
        buttonClick: function() {
            this.model.get('click')();
        }
    });
});