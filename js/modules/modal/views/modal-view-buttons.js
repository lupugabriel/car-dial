define(function(require, exports, module) {
    var Marionette = require('marionette');
    var ModalViewButtonsItem = require('modules/modal/views/modal-view-buttons-item');
    var app = require('app');

    module.exports = Marionette.CollectionView.extend({
        itemView: ModalViewButtonsItem,
        initialize: function() {
            this.collection.add(new Backbone.Model({
                type: 'default',
                label: 'Close',
                icon: 'chevron-up',
                click: function() {
                    app.modal.hideModal();
                    Backbone.history.history.back();
                }
            }))
        }
    });
});