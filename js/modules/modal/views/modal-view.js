define(function(require, exports, module) {
    var $ = require('jquery');
    var Marionette = require('marionette');
    var ButtonsView = require('modules/modal/views/modal-view-buttons');

    module.exports = Marionette.Layout.extend({
        template: '#modal-view',
        ui: {
        	$modalTitle: '#appModalTitle',
            $modalBody: '#appModalBody',
        	$modal: '#appModal'
        },
        regions: {
            modalBody: '#appModalBody',
            modalFooter: '#appModalFooter'
        },
        showModal: function( options ) {
            if ( options ) {
                this.ui.$modalTitle.html( this.getTitle(options.title) );

                if ( typeof options.body === 'string' ) {
                    this.ui.$modalBody.html( options.body );
                } else if ( options.body instanceof Backbone.View ) {
                    this.modalBody.show( options.body );
                }

                this.modalFooter.show(new ButtonsView({
                    collection: options.buttons
                }));
            }

            this.ui.$modal.modal('show');
        },
        getTitle: function( title ) {
            var output = "";
            if ( title.icon ) {
                output += _.template('<i class="glyphicon glyphicon-<%- icon %>"></i> ', title);
            }
            return output + title.label;
        },
        hideModal: function() {
            this.ui.$modal.modal('hide');
        }
    });
});