define(function(require, exports, module) {
    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    var Marionette = require('marionette');
    var NavbarView = require('modules/navigation/views/navbar');

    module.exports = Marionette.Controller.extend({
        initialize: function () {
            this.navbarView = new NavbarView();
        },
        setActiveMenuItem: function( menuItem ) {
            this.navbarView.ui.$menuButtons.removeClass('active');
            var menuItem = this.navbarView.$('.nav-' + menuItem);
            if ( menuItem.length ) {
                menuItem.addClass('active');
            } else {
                this.navbarView.$('.nav-showmap').addClass('active');
            }
        }
    });
});